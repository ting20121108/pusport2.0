<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LocationModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function store($lat, $long)
    {
        $data = [
			'latitude' => $lat,
			'longitude' => $long,
		];

        $this->db->insert('gps', $data);
        $result = $this->db->affected_rows();

        if ($result == 1)
        {
            return "success";
        }
    }
}