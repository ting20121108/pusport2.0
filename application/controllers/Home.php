<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
			parent::__construct();
			$this->load->helper('url');
			$this->load->model('LocationModel');
	}

	public function index()
	{
		$this->load->view('Header');
		$this->load->view('index');
	}

	public function store()
	{
		$lat = $this->input->post("latitude");
		$long = $this->input->post("longitude");
		$result = $this->LocationModel->store($lat, $long);
		if($result != "success")
		{
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode([
				'status' => 'fail',
			], JSON_UNESCAPED_UNICODE));
		}else{
			$this->output->set_output(json_encode([
				'status' => 'success',
			], JSON_UNESCAPED_UNICODE));
		}
	}
}