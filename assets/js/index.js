$(document).ready(function() {

    // 檢查是否支援定位
    if (navigator.geolocation) {
        console.log('支援 Geolocation');
        geolocation()
    } else {
        console.log('不支援 Geolocation');
    }

    function geolocation() {

        function success(pos) {
            var crd = pos.coords;

            let lat = document.createElement("lat");
            let long = document.createElement("long");
            lat.textContent = crd.latitude;
            long.textContent = crd.longitude;
            document.querySelector('#latitude').appendChild(lat);
            document.querySelector('#longitude').appendChild(long);

            store(crd);

            console.log('Your current position is:');
            console.log('Latitude : ' + crd.latitude);
            console.log('Longitude: ' + crd.longitude);
            console.log('More or less ' + crd.accuracy + ' meters.');
        };

        function error(err) {
            console.warn('ERROR(' + err.code + '): ' + err.message);
        };

        navigator.geolocation.getCurrentPosition(success, error);

    };

    function store(crd) {
        $latitude = crd.latitude;
        $longitude = crd.longitude;
        $.ajax({
            type: "POST",
            url: base_url + "index.php/Home/store",
            data: {
                'latitude': $latitude,
                'longitude': $longitude,
            },
            dataType: "JSON",
            success: function(response) {
                console.log(response);
                if (response['status'] != "success") {
                    alert("錯誤！未成功存入目前位置");
                } else {
                    alert("成功存入目前位置");
                }
            }
        });
    }

});